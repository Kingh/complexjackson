package com.zencode;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.InvalidTypeIdException;
import com.sun.jdi.InvalidTypeException;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;

public class ExtractValue {
    private static ArrayList<JsonNode> res = new ArrayList<>(10);

    public static<T> ArrayList<JsonNode> extract(T input, String key) throws IOException, InvalidTypeException {
         Iterator<Map.Entry<String,JsonNode>> fieldsIterator = parseInput(input).fields();
         while (fieldsIterator.hasNext()) {
             Map.Entry<String,JsonNode> field = fieldsIterator.next();
             JsonNode fieldValue = field.getValue();
             String fieldKey = field.getKey();

             if (fieldKey.equals(key)) {
                 res.add(fieldValue);
             } else if (fieldValue.isArray()) {
                 for (JsonNode elem: fieldValue) {
                     if (elem.isObject()) {
                         if (elem.has(key)) {
                             res.add(elem.findValue(key));
                         }else {
                             extract(elem.toString(), key);
                         }
                     }
                 }
             } else if (fieldValue.isObject()) {
                 extract(fieldValue.toString(), key);
             }
         }

         return res;
     }

     private static<T> JsonNode parseInput(T input) throws IOException, InvalidTypeException {
         JsonFactory factory = new JsonFactory();
         ObjectMapper mapper = new ObjectMapper(factory);
         JsonNode node = null;

         if (input instanceof File) {
             node = mapper.readTree((File)input);
         } else if (input instanceof String) {
             node = mapper.readTree((String)input);
         } else if (input instanceof URL) {
             node = mapper.readTree((URL)input);
         } else {
             throw new InvalidTypeException();
         }

         return node;
     }
}
