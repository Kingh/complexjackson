package com.zencode;

import com.sun.jdi.InvalidTypeException;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class Client {
    public static void main(String[] args) {
        try {
            System.out.println(ExtractValue.extract(new URL("http://dummy.restapiexample.com/api/v1/employees"), "employee_name"));
        } catch (IOException | InvalidTypeException e) {
            e.printStackTrace();
        }
    }
}
